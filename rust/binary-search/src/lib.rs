pub fn find(array: &[i32], key: i32) -> Option<usize> {
    let mut start_index = 0;
    let mut end_index = array.len();
    
    while start_index != end_index {
        let middle_index = (start_index + end_index) / 2;
        let middle = array[middle_index];
        match key > middle {
            false if key == middle => {
                return Some(middle_index);
            },
            false => {
                end_index = middle_index;
            },
            true => {
                start_index = middle_index + 1;
            },            
        }
    };

    None
}
