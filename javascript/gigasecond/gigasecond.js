module.exports = class Gigasecond {
  constructor(date) {
    this.input = date;
  }

  date() {
    return new Date(+this.input + 10**9 * 1000);
  }
}
