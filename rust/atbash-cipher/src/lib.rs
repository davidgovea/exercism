/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {
    let encoded = perform_cipher(plain);

    // Split encoded payload into 5-character chunks
    encoded
        .chunks(5)
        .map(|char_list| char_list.into_iter().collect::<String>())
        .collect::<Vec<_>>()
        .join(" ")
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
    perform_cipher(cipher)
        // Create String from Vec<char>
        .into_iter().collect()
}

// ===================================================

// Generic enc/dec cipher function
fn perform_cipher(input: &str) -> Vec<char> {
    input.to_lowercase().chars()
        // Filter out whitespace and non-ascii chars
        .filter(|c| c.is_alphanumeric() && c.is_ascii())
        .map(rotate_char)
        .collect::<Vec<char>>()
}

// Rotates alphabetic characters (does not touch numbers)
fn rotate_char(c: char) -> char {
    match c.is_alphabetic() {
        true => {
            // This could be better - would be nice to store the character mapping,
            //  rather than recreating on each character rotation
            let list = "abcdefghijklmnopqrstuvwxyz";
            let mut map = list.chars().zip(list.chars().rev());

            let (_, rotated_char) = map.find(|&(fwd, _rev)| fwd == c).unwrap();
            rotated_char
        },

        false => c
    }
}
