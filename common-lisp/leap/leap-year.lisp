(defpackage #:leap
  (:use #:common-lisp)
  (:export #:leap-year-p))
(in-package #:leap)

(defun leap-year-p (year)
  ; "Basic" leap years are divisible-by-4
  (setq isBasicLeap (zerop (rem year 4)))
  ; "Exception" non-leap years are divisible by 100, but not 400
  (setq isException (and (zerop (rem year 100)) (plusp (rem year 400))))

  (and isBasicLeap (not isException))
  )
