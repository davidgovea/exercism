pub fn encode(input: &str) -> String {
    let mut last_seen = None;
    let mut count = 0;
    let mut chars = input.chars();

    (0..input.len() + 1).fold(String::new(), |output, _| {
        let c = chars.next();
        let previous_last = last_seen;
        last_seen = c;

        output + &match previous_last == c {
            true => {
                count += 1;
                "".to_string()
            },
            false => match (previous_last, count) {
                (Some(c), 0) => format!("{}", c),
                (Some(c), _) => {
                    let prev_count = count;
                    count = 0;
                    format!("{}{}", prev_count + 1, c)
                },
                _ => "".to_string()
            }
        }
    })
}


pub fn decode(input: &str) -> String {
    let mut count_string = String::new();

    input.chars().fold(String::new(), |output, c| {
        output + &match (!c.is_digit(10), count_string.is_empty()) {
            (true, true) => {
                c.to_string()
            },
            (true, false) => {
                let count: u32 = count_string.parse().unwrap();
                count_string = String::new();
                (0..count).map(|_| c).collect()
            },
            (false, _) => {
                count_string += &c.to_string();
                "".to_string()
            }
        }
    })
}
