/// Compute the Scrabble score for a word.
pub fn score(word: &str) -> u64 {
  word
    .chars()
    .filter(|c| c.is_alphabetic() && c.is_ascii())
    .map(letter_score)
    .sum()
}

fn letter_score(c: char) -> u64 {
  let uppercase_char = c.to_uppercase().next().unwrap();
  match uppercase_char {
    'A'|'E'|'I'|'O'|'U'|'L'|'N'|'R'|'S'|'T' => 1,
    'D'|'G'                                 => 2,
    'B'|'C'|'M'|'P'                         => 3,
    'F'|'H'|'V'|'W'|'Y'                     => 4,
    'K'                                     => 5,
    'J'|'X'                                 => 8,
    'Q'|'Z'                                 => 10,
    _                                       => 0,
  }
}
