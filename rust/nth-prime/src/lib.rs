
pub fn is_prime(n: usize) -> bool {

    // Start testing factors at 2
    for i in 2..(((n as f32).sqrt() as usize) + 1) {
        if n % i == 0 {
            return false;
        }
    }

    return true;
}

#[derive(Debug, PartialEq)]
pub enum PrimeError {
    NoZerothPrime
}

pub fn nth(n: usize) -> Result<usize, PrimeError> {
    match n {
        // Input is zero: error!
        0 => Err(PrimeError::NoZerothPrime),
        _ => {
            let mut candidate = 2;

            for _ in 1..n {
                candidate += 1;
                while !is_prime(candidate) {
                    candidate += 1;
                }
            }

            Ok(candidate)
        }
    }
}
