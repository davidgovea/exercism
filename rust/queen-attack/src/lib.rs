use std::collections::HashMap;

#[derive(PartialEq, Debug)]
pub struct ChessPosition {
    x: i32,
    y: i32,
}

#[derive(Debug)]
pub struct Queen {
    pos: ChessPosition,
}

const BOARD_SIZE: i32 = 8;

impl ChessPosition {
    pub fn new(rank: i32, file: i32) -> Option<Self> {
        match (rank + 1, file + 1) {
            (1 ... BOARD_SIZE, 1 ... BOARD_SIZE) => Some(ChessPosition {
                x: rank,
                y: file,
            }),
            _ => None
        }
    }
}

impl Queen {
    pub fn new(position: ChessPosition) -> Self {
        Queen {
            pos: position
        }
    }

    pub fn can_attack(&self, other: &Queen) -> bool {
        let directions = [
            (1, 0),
            (-1, 0),
            (0, 1),
            (0, -1),
            (1, 1),
            (-1, 1),
            (1, -1),
            (-1, -1),
        ];

        for (x, y) in &directions {
            let mut current_pos = ChessPosition::new(self.pos.x, self.pos.y).unwrap();
            while let Some(pos) = ChessPosition::new(current_pos.x + x, current_pos.y + y) {
                if pos == other.pos {
                    return true;
                }
                current_pos = pos;
            }
        }

        false
    }
}
