#[derive(Debug)]
#[derive(PartialEq)]
pub struct Clock {
    minutes: usize
}

impl Clock {
    pub fn new(hours: isize, minutes: isize) -> Clock {
        let mut total_minutes = hours * 60 + minutes;
        while total_minutes < 0 {
            total_minutes += 1440;
        }

        Clock { minutes: (total_minutes % 1440) as usize }
    }

    pub fn add_minutes(&mut self, minutes: isize) -> Clock {
        // Should I be returning self here, rather than a new Clock?
        Clock::new(0, self.minutes as isize + minutes)
    }

    // Need to look into display trait
    pub fn to_string(&self) -> String {
        let hours = self.minutes / 60;
        let minutes = self.minutes % 60;

        format!("{:02}:{:02}", hours, minutes)
    }
}
