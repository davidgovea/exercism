/// Convert a list of numbers to a stream of bytes encoded with variable length encoding.
pub fn to_bytes(values: &[u32]) -> Vec<u8> {
    values.iter().fold(vec![], |mut list, value| {
        let mut processing = *value;
        let mut signal_bit = 0;
        let mut this_number = vec![];

        while processing > 0 || signal_bit == 0 {
            // Grab 7 bits from number
            let seven_bits = processing & 0b01111111;
            // Drop those 7 bits
            processing >>= 7;
            // Assemble result byte
            let result_byte = (signal_bit << 7) + seven_bits as u8;
            // Add to the start of this_number list
            this_number.insert(0, result_byte);
            // Flip signal bit after first round
            signal_bit = 1;
        }

        list.append(&mut this_number);
        list
    })
}

/// Given a stream of bytes, extract all numbers which are encoded in there.
pub fn from_bytes(bytes: &[u8]) -> Result<Vec<u32>, &'static str> {
    // Boolean to track "more coming" bit
    let mut data_pending = false;
    // Boolean to track integer overflow (needs improvement)
    let mut did_overflow = false;

    let numbers = bytes.iter().fold(vec![], |mut list, value| {
        // Grab the 7 bits of number content
        let number_segment = (value & 0b01111111) as u32;

        match data_pending {
            true => {
                // Grab the number we're assembling
                let mut last = list.pop().unwrap_or(0);

                // Pad with 7 zeros - will we overflow?
                if last & 0xFE000000 != 0 {
                    did_overflow = true;
                }
                last <<= 7;

                // Add this segment
                last = last + number_segment;
                // Put it back on the list
                list.push(last);
            },
            false => {
                // Fresh number, add it to list
                list.push(number_segment);
            }
        };

        // Set pending flag to use on the next byte processed
        data_pending = value >> 7 == 1u8;
        list
    });

    match (data_pending, did_overflow) {
        (false, false) => Ok(numbers),
        (true, _) => Err("Bad stream"),
        (_, true) => Err("Overflow"),
    }
}
