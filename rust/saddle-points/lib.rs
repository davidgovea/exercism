#![feature(iterator_for_each)]
// Definitely over-iterating here by computing Row-Max and Col-Min separately
pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    input.iter().enumerate().fold(vec![], |mut list, (row_index, row)| {
        match row.iter().max() {
            Some(row_max) => {
                row.iter().enumerate().for_each(|(col_index, el)| {
                    if el == row_max {
                        let col_min = input.iter().map(|row| row[col_index]).min().unwrap();
                        if row[col_index] == col_min {
                            list.push((row_index, col_index));
                        }
                    }
                });
            },
            None => {}
        };

        list
    })
}

pub fn find_saddle_points_first(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let num_rows = input.len();
    let num_cols = input[0].len();

    // This doesn't feel quite right
    if num_rows == 0 || num_cols == 0 {
        return vec![];
    }

    let row_maxs = input.iter()
        .map(|row| row.iter().max().unwrap())
        .collect::<Vec<_>>();

    let col_mins = (0..num_cols)
        .map(|col_index| {
            let col = (0..num_rows).map(|row_index| input[row_index][col_index]).collect::<Vec<_>>();
            *col.iter().min().unwrap()
        })
        .collect::<Vec<_>>();

    (0..num_rows).fold(vec![], |mut list, row_index| {
        for col_index in 0..num_cols {
            if &input[row_index][col_index] == row_maxs[row_index] && &input[row_index][col_index] == &col_mins[col_index] {
                list.push((row_index, col_index));
            }
        }
        list
    })
}

pub fn find_saddle_points_bkhl(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let mut result = Vec::new();

    for y in 0..input.len() {
        for x in 0..input[y].len() {
            let point = Some(&input[y][x]);

            if input[y].iter().max() == point
                && input.iter().map(|v| &v[x]).min() == point
            {
                result.push((y, x));
            }
        }
    }

    result
}

pub fn find_saddle_points_mark(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    // Nothing to do if we have no rows, or if the rows are empty
    if input.is_empty() || input[0].is_empty() {
        return Vec::new();
    }

    // Find the maximum value in each row
    // We will scan columns below looking for the minimum values, and can simply compare this with these maximums to determine a saddle
    let max_by_row = input.iter().map(|row| row.iter().max().unwrap()).collect::<Vec<&u64>>();

    // For each column, find the rows with the minimum values
    // If the minimum equals the maximum value flagged above, the cell is a saddle
    let mut saddles = Vec::new();
    let num_cols = input[0].len();
    for col_num in 0..num_cols {
        // Find the minimum value in this column
        let column = input.iter().map(|row| row[col_num]).collect::<Vec<u64>>();
        let col_min = column.iter().min().unwrap();

        // Scan the column again and for any values equal to the maximum above, we have a saddle
        column.iter().enumerate()
            .filter(|&(row_num, value)| *value == *col_min && *value == *max_by_row[row_num])
            .for_each(|(row_num, _value)| saddles.push((row_num, col_num)));
    }

    saddles
}

fn get_row(rowi: usize, num_rows: usize, matrix: &[Vec<u64>]) -> Vec<&u64> {
    assert!(rowi < num_rows, "row index overflow");

    (&matrix[rowi]).iter().collect()
}

fn get_col(coli: usize, num_cols: usize, matrix: &[Vec<u64>]) -> Vec<&u64> {
    assert!(coli < num_cols, "col index overflow");

    (0..matrix.len()).map(|rowi| &(matrix[rowi][coli])).collect()
}

fn is_saddle_point(point: (usize, usize), num_rows: usize, num_cols: usize, matrix: &[Vec<u64>]) -> bool {
    let point_value: u64 = matrix[point.0][point.1];

       get_row(point.0, num_rows, matrix).iter().all(|&e| *e <= point_value)
    && get_col(point.1, num_cols, matrix).iter().all(|&e| *e >= point_value)

}
// http://exercism.io/submissions/3a694123facf4d60b53559e16d52183d
pub fn find_saddle_points_bmer(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let num_rows: usize = input.len();
    let num_cols: usize = if num_rows == 0 { 0 } else { input[0].len() };
    let mut saddle_points: Vec<(usize, usize)> = vec![];

    for i in 0..num_rows {
        for j in 0..num_cols {
            if is_saddle_point((i, j), num_rows, num_cols, input) {
                saddle_points.push((i, j));
            }
        }
    }

    saddle_points
}

use std::u64;
// http://exercism.io/submissions/978b1b73788b48c789343fd93c725ea2
pub fn find_saddle_points_pmj(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let rows = input.len();
    let cols = input[0].len();
    let mut row_max = vec![u64::MIN; rows];
    let mut col_min = vec![u64::MAX; cols];
    for (i, row) in input.iter().enumerate() {
        for (j, cell) in row.iter().enumerate() {
            if *cell > row_max[i] {
                row_max[i] = *cell;
            }
            if *cell < col_min[j] {
                col_min[j] = *cell;
            }
        }
    }
    let mut saddles = Vec::new();
    for (i, row) in input.iter().enumerate() {
        for (j,  cell) in row.iter().enumerate() {
            if *cell == row_max[i] && *cell == col_min[j] {
                saddles.push((i,j));
            }
        }
    }
    saddles
}
// http://exercism.io/submissions/f85cea268b394a99a83fcbde2bbb2354
pub fn find_saddle_points_ryym(matrix: &[Vec<u64>]) -> Vec<(usize, usize)> {
    if !is_valid_matrix(matrix) {
        panic!("This is not a matrix");
    }

    let n_rows = matrix.len();
    let n_cols = matrix[0].len();
    if n_cols == 0 {
        return Vec::new();
    }

    let max_cols: Vec<u64> = matrix.iter()
        .map(|row| *row.iter().max().unwrap())
        .collect();

    let min_rows: Vec<u64> = (0..n_cols)
        .map(|c| (0..n_rows).map(|r| matrix[r][c]).min().unwrap())
        .collect();

    let mut saddles = Vec::new();
    for r in 0..n_rows {
        for c in 0..n_cols {
            let v = matrix[r][c];
            if v == max_cols[r] && v == min_rows[c] {
                saddles.push((r, c));
            }
        }
    }
    saddles
}

fn is_valid_matrix(matrix: &[Vec<u64>]) -> bool {
    if matrix.len() == 0 {
        false
    } else {
        let n_cols = matrix[0].len();
        matrix.iter().all(|row| row.len() == n_cols)
    }
}

// http://exercism.io/submissions/f08ef987f517454eaaf181d6420893e4
pub fn find_saddle_points_miken(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let mut v = Vec::new();
    for (i,row) in input.iter().enumerate() {
        let maxval;
        match row.iter().max() {
            Some(x) => maxval = x,
            None => break,
        }
        'con: for (maxc,_) in row.iter().enumerate()
                .filter(|&(_,val)| val == maxval) {
            for col in input {
                if col[maxc] < row[maxc] {
                    continue 'con;
                }
            }
            v.push((i,maxc));
        }
    }
    v
}

// http://exercism.io/submissions/defba5f5551b430d933bdd2e8b585a87
type Point = (usize, usize);
pub fn find_saddle_points_oliveruv(input: &[Vec<u64>]) -> Vec<Point> {

    // Assume rectangular
    let height = input.len();
    let width = input[0].len();

    // For this solution we also assume that there are never multiple
    // cells that share the property of being greatest in row or least
    // in column, unless every cell in that row or column has the same
    // value.

    // We go through input twice, once by row and once by column.
    // We look for candidates that fulfill the row or column wise
    // conditions for being saddle points. We increase a cell's score
    // if we know it's a candidate in a row or a column. If a cell is
    // the correct candidate in both its row and column, we know it's a
    // saddle point.

    // This solution requires us to use once again the amount of memory
    // of the input, but we are guaranteed to iterate only twice through
    // the input grid, and once through the score grid. This is
    // probably more cache efficient than other similar solutions (e.g.
    // http://exercism.io/submissions/f114aaa2662c419dac2779945d822ccb )

    let mut score_grid = vec![vec![0; width]; height];

    // find greatest (or all equal) points in each row
    for (r, row) in input.iter().enumerate() {
        let mut all_same = true;
        let mut greatest_val = 0u64;
        let mut greatest_pos: usize = 0;
        let mut first = true;
        for (c, val) in row.iter().enumerate() {
            if first {
                first = false;
                greatest_val = *val;
                greatest_pos = c;
                continue;
            }
            if *val != row[0] {
                all_same = false;
            }
            if *val > greatest_val {
                greatest_pos = c;
                greatest_val = *val;
                continue;
            }
        }
        if all_same {
            for c in 0..width {
                score_grid[r][c] += 1;
            }
        } else {
            score_grid[r][greatest_pos] += 1;
        }
    }

    // find least (or all equal) points in each column
    for c in 0..width {
        let mut all_same = true;
        let mut least_val = 0u64;
        let mut least_pos: usize = 0;
        let mut first = true;
        for r in 0..height {
            let val = input[r][c];
            if first {
                first = false;
                least_val = val;
                least_pos = r;
                continue;
            }
            if val != input[0][c] {
                all_same = false;
            }
            if val < least_val {
                least_pos = r;
                least_val = val;
                continue;
            }
        }
        if all_same {
            for r in 0..height {
                score_grid[r][c] += 1;
            }
        } else {
            score_grid[least_pos][c] += 1;
        }
    }

    let mut res = vec!();
    for r in 0..height {
        for c in 0..width {
            if score_grid[r][c] == 2 {
                res.push((r, c));
            }
        }
    }

    res
}

// Probably best solution I've seen so far. Fewer iterations, less
// memory usage, easier to understand code:
// http://exercism.io/submissions/f85cea268b394a99a83fcbde2bbb2354
// Same but closer to C-style:
// http://exercism.io/submissions/978b1b73788b48c789343fd93c725ea2

// Also interesting, as usual: http://exercism.io/submissions/f08ef987f517454eaaf181d6420893e4
// It seems to sacrifice some computation to save on memory, compared to my solution
