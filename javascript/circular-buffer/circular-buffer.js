export default function shim(length) {
  return new CircularBuffer(length);
}

export class CircularBuffer {
  constructor(length) {
    this.data = new Array(length);
    this.readIndex = 0;
    this.writeIndex = 0;
    this.full = false;
  }

  read() {
    if (this.isEmpty) {
      throw new BufferEmptyError();
    }
    const val = this.data[this.readIndex];
    this.readIndex = (this.readIndex + 1) % this.data.length;
    this.full = false;
    return val;
  }

  write(val) {
    if (this.full) {
      throw new BufferFullError();
    }
    return this._write(val);
  }

  clear() {
    this.readIndex = 0;
    this.writeIndex = 0;
    this.full = false;
  }

  forceWrite(val) {
    if (this.full && val != null) {
      this.readIndex = (this.readIndex + 1) % this.data.length;
    }
    return this._write(val);
  }

  _write(val) {
    if (val == null) {
      return;
    }

    this.data[this.writeIndex] = val;
    if (this.isLastAvailable) {
      this.full = true;
    }
    this.writeIndex = (this.writeIndex + 1) % this.data.length;
  }

  get isEmpty() {
    return this.readIndex === this.writeIndex && !this.full;
  }

  get isLastAvailable() {
    return this.readIndex === ((this.writeIndex + 1) % this.data.length);
  }
}

export class BufferEmptyError extends Error {}
export class BufferFullError extends Error {}
