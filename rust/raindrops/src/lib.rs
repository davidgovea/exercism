pub fn raindrops(n: usize) -> String {
    let mut output = String::new();

    if n % 3 == 0 {
        output += "Pling";
    }

    if n % 5 == 0 {
        output += "Plang";
    }

    if n % 7 == 0 {
        output += "Plong";
    }

    match output.is_empty() {
        // Matched no factors - return n as string
        true => n.to_string(),
        // Return prepared rain sounds
        false => output
    }
}
