pub fn build_proverb(list: Vec<&str>) -> String {

    match list.first() {
        Some(first) => {

            let mut proverb = list
                // Zip offset `rest` with original list to create verse pairs
                .windows(2)
                // Map verses
                .map(|pair| {
                    format!("For want of a {} the {} was lost.", pair[0], pair[1])
                })
                .collect::<Vec<_>>();

            // Add final sentence to list of lines
            proverb.push(format!("And all for the want of a {}.", first));

            // Return the joined string
            proverb.join("\n")
        },

        // The list was empty - just return a blank string
        None => String::new()
    }
}
