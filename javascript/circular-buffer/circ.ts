export default function shim(length) {
  return new CircularBuffer(length);
}

export class CircularBuffer {
  data: any[];
  readIndex: number;
  writeIndex: number;

  constructor(length: string) {
    this.data = new Array(length);
    this.readIndex = 0;
    this.writeIndex = 0;
  }

  read(): any {
    if (this.isEmpty) {
      throw new BufferEmptyError();
    }
  }

  write(val: any) {
    if (this.isFull) {
      throw new BufferFullError();
    }
    return this._write(val); 
  }

  clear() {}

  forceWrite(val: any) {
    if (this.isFull) {
      this.readIndex++;
    }
    return this._write(val);
  }

  private _write(val: any) {
    this.data[this.writeIndex++] = val;
    return val;
  }

  get isEmpty() {
    return this.readIndex === this.writeIndex;
  }

  get isFull() {
    return this.readIndex === ((this.writeIndex + 1) & this.data.length);
  }
}

export class BufferEmptyError extends Error {}
export class BufferFullError extends Error {}
