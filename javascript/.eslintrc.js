module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: '2017',
    sourceType: 'module'
  },
  extends: 'eslint:recommended',
  env: {
    node: true,
    es6: true,
  },
  rules: {
    'comma-dangle': 0,
    'prefer-const': 1,
    'no-constant-condition': 0,
  },
};
