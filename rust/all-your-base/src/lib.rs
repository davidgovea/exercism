#[derive(Debug, PartialEq)]
pub enum Error {
    InvalidInputBase,
    InvalidOutputBase,
    InvalidDigit(u32),
}

pub fn convert(number: &[u32], from_base: u32, to_base: u32) -> Result<Vec<u32>, Error> {
    match from_base > 1 {
        true => {},
        _ => {
            return Err(Error::InvalidInputBase);
        }
    }
    match to_base > 1 {
        true => {},
        _ => {
            return Err(Error::InvalidOutputBase);
        }
    }

    number.iter().try_for_each(|d| {
        match d >= &from_base {
            true => Err(Error::InvalidDigit(*d)),
            false => Ok(())
        }
    })?;

    let num = number.into_iter().rev().enumerate().fold(0, |sum, (i, d)| {
        let digit_value = (from_base as f64).powi(i as i32) as u32;
        sum + *d * digit_value
    });

    let new_digit_places = (num as f64).log(to_base as f64) as u32;

    let new_list = (0..=new_digit_places).rev().fold((Vec::new(), num.clone()), |(mut list, remaining), digit_index| {
        let digit_value = (to_base as f64).powi(digit_index as i32) as u32;
        let digit_total = ((remaining as f64) / (digit_value as f64)) as u32;
        if (digit_total != 0 || list.len() > 0) {
        list.push(digit_total);
        }
        (list, remaining - digit_total * digit_value)
    }).0;
    
    Ok(new_list)
}
