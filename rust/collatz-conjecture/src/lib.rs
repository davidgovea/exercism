pub fn collatz(mut n: u64) -> Result<u64, &'static str> {
    if n <= 0 {
        return Err("Must be a positive integer");
    }

    let mut steps = 0;

    while n > 1 {
        if n % 2 == 0 {
            n = n / 2;
        } else {
            n = n * 3 + 1;
        }
        steps += 1;
    }
    Ok(steps)
}
