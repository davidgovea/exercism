const rnaMap = {
  G: 'C',
  C: 'G',
  T: 'A',
  A: 'U',
}

module.exports = class DnaTranscriber {
  toRna(dna) {
    return dna
      .split('')
      .map((char) => {
        const replacedChar = rnaMap[char];
        if (replacedChar) {
          return replacedChar;
        }

        throw new Error('Invalid input');
      })
      .join('');
  }
}
