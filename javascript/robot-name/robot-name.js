// Configurable character set
const START_CODE = 'A'.charCodeAt(0);
const END_CODE = 'Z'.charCodeAt(0);

// Utilities to map 0-1 values to character or digit
const generateChar = (rand) => String.fromCharCode(START_CODE + Math.floor(rand * (END_CODE - START_CODE)));
const generateInt  = (rand) => Math.floor(rand * 10);

// Registry to track existing names
const robotRegistry = new Set();

module.exports = class Robot {
  constructor() {
    this.reset();
  }

  _generateName() {
    const randomVals = Array(5).fill(null).map(() => Math.random());

    return [
      ...randomVals.slice(0, 2).map(generateChar), // 2 chars
      ...randomVals.slice(2, 5).map(generateInt),  // 3 digits
    ].join('');
  }

  reset() {
    const name = this._generateName();

    // Add new name, then check Set size
    const knownRobots = robotRegistry.size;
    robotRegistry.add(name);

    if (robotRegistry.size > knownRobots) {
      // Size changed, this is a valid name
      this.name = name;
    } else {
      // Try again
      return this.reset();
    }

  }
}
