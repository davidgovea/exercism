module.exports = class Binary {
  constructor(binaryString = '') {
    this.value = binaryString;
  }

  toDecimal() {
    let result = 0;

    if (this.value.match(/^[01]+$/g)) {
      for (const [index, digit] of this.value.split('').reverse().entries()) {
        const num = parseInt(digit);
        result += num * 2**index;
      }
    }

    return result;
  }
}
