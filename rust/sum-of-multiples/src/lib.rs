pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    let mut multiple_list = factors.iter().fold(vec![], |mut list, &i| {
        let mut multiple = i;
        while multiple < limit {
            list.push(multiple);
            multiple += i;
        }
        list
    });

    multiple_list.sort();
    multiple_list.dedup();
    multiple_list.iter().sum()
}
