const secretCommands = [
  [2 ** 0, (list) => [...list, 'wink']],
  [2 ** 1, (list) => [...list, 'double blink']],
  [2 ** 2, (list) => [...list, 'close your eyes']],
  [2 ** 3, (list) => [...list, 'jump']],
  [2 ** 4, (list) => list.reverse()],
];

module.exports = class SecretHandshake {
  constructor(input) {
    if (!Number.isFinite(input)) {
      throw new Error('Handshake must be a number');
    }

    this.commands = () => secretCommands.reduce((list, [trigger, mutator]) => {
      if (input & trigger) {
        list = mutator(list);
      }
      return list;
    }, []);
  }
}
