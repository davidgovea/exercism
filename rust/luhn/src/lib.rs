/// Check a Luhn checksum.
enum LuhnError {
    InvalidCharacter
}

fn luhn_transform(digit_reverse_index: usize, digit: usize) -> usize {
    match digit_reverse_index % 2 != 0 { // Should double?
        // Simple, no double
        false => digit,
        // Double with overflow protection (minus 9)
        true if digit > 4 => digit * 2 - 9,
        // Double digit
        true => digit * 2
    }
}

pub fn is_valid(code: &str) -> bool {
    let sum_and_digit_count = code.chars()
        .rev()
        .try_fold((0, 0), |(total, digit_count), c| {
            // Try parsing the character as an int
            let parsed_digit = c.to_string().parse::<usize>();

            match parsed_digit {
                // Not an int! If it's anything but whitespace, short-circuit the iterator with error
                Err(_) if !c.is_whitespace() => Err(LuhnError::InvalidCharacter),
                // It's just whitespace. Proceed to the next character
                Err(_) => Ok((total, digit_count)),
                // Compute digit's transform, and add to accumulated total. Also increment digit count
                Ok(n) => Ok((total + luhn_transform(digit_count, n), digit_count + 1))
            }
        });

    match sum_and_digit_count {
        Ok((sum, digit_count)) => digit_count > 1 && sum % 10 == 0, // Check length, validate sum
        Err(_) => false
    }

}
