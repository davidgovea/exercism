const EARTH_YEAR_SECONDS = 31557600;

class SpaceAge {
  constructor(seconds) {
    this.seconds = seconds;
  }
}

SpaceAge.prototype.onEarth = planetAgeMacro(1);
SpaceAge.prototype.onMercury = planetAgeMacro(0.2408467);
SpaceAge.prototype.onVenus = planetAgeMacro(0.61519726);
SpaceAge.prototype.onMars = planetAgeMacro(1.8808158);
SpaceAge.prototype.onJupiter = planetAgeMacro(11.862615);
SpaceAge.prototype.onSaturn = planetAgeMacro(29.447498);
SpaceAge.prototype.onUranus = planetAgeMacro(84.016846);
SpaceAge.prototype.onNeptune = planetAgeMacro(164.79132);

function planetAgeMacro(orbitInYears) {
  return function() {
    const age = this.seconds / (orbitInYears * EARTH_YEAR_SECONDS);
    return round(age, 2);
  }
}

function round(value, decimals) {
  // Better than toFixed(2): http://www.jacklmoore.com/notes/rounding-in-javascript/
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

module.exports = SpaceAge;
