#![feature(test)]
extern crate diffie_hellman;
extern crate test;

use test::Bencher;
use diffie_hellman::*;

/*
Results on my machine:

without early continue: 635,377 ns/iter (+/- 117,637)
with early continue: 721,473 ns/iter (+/- 94,478) WHAT?!
euclid's formula: 859 ns/iter (+/- 447)
*/

#[bench]
fn bench_answer(b: &mut Bencher) {
    b.iter(|| {
        let p: u64 = 31;
        let g: u64 = 19;

        let private_key_a = private_key(p);
        let private_key_b = private_key(p);

        let public_key_a = public_key(p, g, private_key_a);
        let public_key_b = public_key(p, g, private_key_b);

        // Key exchange
        let secret_a = secret(p, public_key_b, private_key_a);
        let secret_b = secret(p, public_key_a, private_key_b);

        assert_eq!(secret_a, secret_b);
    })
}
