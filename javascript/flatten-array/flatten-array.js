export default class Flattener {
  flatten(arr) {
    return arr.reduce((flatArr, item) => {
      if (Array.isArray(item)) {
        return [...flatArr, ...this.flatten(item)];
      }
      if (item == null) {
        return flatArr;
      }

      return [...flatArr, item];
    }, []);
  }
}
