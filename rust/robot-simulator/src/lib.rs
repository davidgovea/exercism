// The code below is a stub. Just enough to satisfy the compiler.
// In order to pass the tests you can add-to or change any of this code.

#[derive(PartialEq, Debug, Copy, Clone)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

pub struct Robot {
    x: i32,
    y: i32,
    d: Direction,
}

static DIRECTIONS: [Direction; 4] = [Direction::North, Direction::East, Direction::South, Direction::West];

impl Robot {
    pub fn new(x: i32, y: i32, d: Direction) -> Self {
        Robot { x, y, d }
    }

    pub fn turn_right(mut self) -> Self {
        self.d = self.rotate_dir(true);
        self
    }

    pub fn turn_left(mut self) -> Self {
        self.d = self.rotate_dir(false);
        self
    }

    fn rotate_dir(&self, clockwise: bool) -> Direction {
        let current_index = DIRECTIONS.iter().position(|d| d == self.direction()).unwrap() as i32;
        let len = DIRECTIONS.len() as i32;
        let new_index = match clockwise {
            true => current_index + 1,
            false => current_index - 1 + len,
        } % len;
        DIRECTIONS[new_index as usize]
    }

    pub fn advance(mut self) -> Self {
        let (move_x, move_y) = match self.direction() {
            &Direction::North => (0, 1),
            &Direction::East => (1, 0),
            &Direction::South => (0, -1),
            &Direction::West => (-1, 0),
        };
        self.x += move_x;
        self.y += move_y;
        self
    }

    pub fn instructions(self, instructions: &str) -> Self {
        let mut robot = self;
        for c in instructions.chars() {
            robot = match c {
                'R' => robot.turn_right(),
                'L' => robot.turn_left(),
                'A' => robot.advance(),
                _ => robot
            };
        };
        robot
    }

    pub fn position(&self) -> (i32, i32) {
        (self.x, self.y)
    }

    pub fn direction(&self) -> &Direction {
        &self.d
    }
}
