export default class Zipper {
  static fromTree(tree) {
    // Quick & dirty deep clone
    return new Zipper(JSON.parse(JSON.stringify(tree)));
  }

  constructor(tree, focus = tree) {
    this.tree = tree;
    this.focus = focus;
  }

  toTree() {
    return this.tree;
  }

  left() {
    const newFocus = this.focus.left;
    this.focus = newFocus;
    return newFocus ? this : null;
  }

  right() {
    const newFocus = this.focus.right;
    this.focus = newFocus;
    return newFocus ? this : null;
  }

  value() {
    return this.focus.value;
  }

  up() {
    return this._topDownParentSearch();
  }

  setValue(val) {
    this.focus.value = val;
    return this;
  }

  setLeft(node) {
    this.focus.left = node;
    return this;
  }

  setRight(node) {
    this.focus.right = node;
    return this;
  }

  _topDownParentSearch(node = this.tree) {
    const { left, right } = node || {};
    if (!left && !right) {
      return null;
    }

    if (left === this.focus || right === this.focus) {
      this.focus = node;
      return this;
    }

    return this._topDownParentSearch(left) || this._topDownParentSearch(right);
  }
}
