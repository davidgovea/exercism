let times; // Declare times operation since we reuse it for 'times' and 'multiplied'
const OPERATIONS = {
  plus: (a, b) => a + b,
  minus: (a, b) => a - b,
  times: times = (a, b) => a * b,
  multiplied: times,
  divided: (a, b) => a / b,
  'to the': (a, b) => a ** b,
};
// Build a RegExp to detect operations and input values
const OPERATION_REGEXP = new RegExp(`(${Object.keys(OPERATIONS).join('|')})[^-\\d]+([-\\d]+)`, 'g');

class WordProblem {
  constructor(question = '') {
    this.question = question;
  }

  answer() {
    try {
      // Use a simple regex to extract starting value
      const [initialVal, index] = this.question.match(/[-\d]+/);

      // Build list of operations by applying global regex multiple times
      const ops = [];
      let tmp;
      while (tmp = OPERATION_REGEXP.exec(this.question.substr(index))) {
        const [, operation, input] = tmp;
        ops.push([operation, parseInt(input, 10)]);
      }

      if (!ops.length) {
        throw new Error();
      }

      // Build final value by applying operations
      return ops.reduce((currentVal, [operation, input]) => {
        return OPERATIONS[operation](currentVal, input);
      }, parseInt(initialVal, 10));

    } catch (e) {
      throw new ArgumentError();
    }
  }
}

class ArgumentError extends Error {}

module.exports = { WordProblem, ArgumentError };
