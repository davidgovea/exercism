#[derive(Debug, PartialEq, Eq)]
pub enum Classification {
    Abundant,
    Perfect,
    Deficient
}

fn sum_factors(num: u64) -> u64 {
    let mut factors: Vec<u64> = vec![];

    for i in 1..(num / 2 + 1) {
        if num % i == 0 {
            factors.push(i);
        }
    }

    factors.iter().fold(0, |sum, i| sum + i)
}



pub fn classify(num: u64) -> Result<Classification, & 'static str> {
    let sum = sum_factors(num);
    match (num > 0, sum > num, sum < num) {
        (false, _, _) => Err("Number must be positive"),
        (_, true, _) => Ok(Classification::Abundant),
        (_, _, true) => Ok(Classification::Deficient),
        (_, false, false) => Ok(Classification::Perfect),
    }
}
