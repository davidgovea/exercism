module.exports = class Triangle {
  constructor(rowCount) {
    this.rows = [...Array(rowCount).keys()].reduce((rows, rowNum) => {

      const previousRow = rows[rowNum - 1] || [1];
      const row = [...Array(rowNum + 1).keys()]
        .map((i) => (previousRow[i] || 0) + (previousRow[i - 1] || 0));

      return [...rows, row];
    }, []);
  }

  get lastRow() {
    return this.rows[this.rows.length - 1];
  }
}
