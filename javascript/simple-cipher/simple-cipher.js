const MIN_CHAR_CODE = "a".charCodeAt(0);
const MAX_CHAR_CODE = "z".charCodeAt(0);
const CIPHER_LENGTH = MAX_CHAR_CODE - MIN_CHAR_CODE + 1;

const ENCODE = Symbol('enc');
const DECODE = Symbol('dec');

function randomKey(digits = 100) {
  return [...Array(digits).keys()]
    .reduce((key) => {
      return key + String.fromCharCode(MIN_CHAR_CODE + Math.floor(Math.random() * CIPHER_LENGTH));
    }, '');
}

module.exports = class Cipher {
  constructor(key = randomKey()) {

    const keyValid = key.length && key.split('').every((c) => {
      return c === c.toLowerCase() && c !== c.toUpperCase();
    });

    if (!keyValid) {
      throw new Error('Bad key');
    }

    this.key = key;
  }

  encode(plaintext = '') {
    return this._perform(plaintext, ENCODE);
  }

  decode(ciphertext) {
    return this._perform(ciphertext, DECODE);
  }

  _perform(text, direction = ENCODE) {
    const preparedText = text.replace(/\s/g, '').toLowerCase();
    const directionFactor = direction === ENCODE ? 1 : -1;
    return preparedText
      .split('')
      .map((c, i) => {
        const keyCode = this.key.charCodeAt(i % this.key.length);
        const keyPosition = keyCode - MIN_CHAR_CODE;

        const charPosition = c.charCodeAt(0) - MIN_CHAR_CODE;

        const newCharPosition = (charPosition + directionFactor*keyPosition + CIPHER_LENGTH) % CIPHER_LENGTH;
        return String.fromCharCode(MIN_CHAR_CODE + newCharPosition);
      })
      .join('');

  }
}
