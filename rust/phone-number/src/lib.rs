pub fn number(user_number: &str) -> Option<String> {
    // Filter non-numeric chars
    let numbers = user_number.chars()
        .filter(|c| {
            c.is_numeric()
        })
        .collect::<Vec<_>>();

    // Reverse number to easily grab area code & exchange code starting digits
    let mut reversed_numbers = numbers.iter().clone().rev();
    let (exchange_code_start, area_code_start) = (reversed_numbers.nth(6), reversed_numbers.nth(2));
    match (area_code_start, exchange_code_start) {
        // Area code cannot start with 0 or 1
        (Some('0' ... '1'), _) => { return None },
        // Exchange code cannot start with 0 or 1
        (_, Some('0' ... '1')) => { return None },
        _ => ()
    };

    // Validate overall length & trim country code if present
    match numbers.len() {
        10 => Some(numbers.into_iter().collect::<String>()),
        11 if numbers.first().unwrap() == &'1' => Some(numbers.into_iter().skip(1).collect::<String>()),
        _ => None
    }
}

