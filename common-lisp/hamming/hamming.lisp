(defpackage #:hamming
  (:use #:cl)
  (:export #:distance))

(in-package #:hamming)

(defun distance (dna1 dna2)
  "Number of positional differences in two equal length dna strands."
  (when (= (length dna1) (length dna2))
    (loop
      for d1 across dna1
      for d2 across dna2
      ; Count non-equal characters
      sum (if (eql d1 d2) 0 1)
    )
  )
)
