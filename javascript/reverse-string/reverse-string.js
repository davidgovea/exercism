module.exports = function reverseString(input) {
  return input.split('').reverse().join('');
}
