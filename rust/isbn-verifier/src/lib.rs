/// Determines whether the supplied string is a valid ISBN number
pub fn is_valid_isbn(isbn: &str) -> bool {

    let chars = isbn.chars()
        .filter(|&c| c != '-')
        .enumerate()
        .filter(|&(index, c)| {
            match (index >= 9, c, c.is_digit(10)) {
                (true, 'X', _) => true,
                (_, _, true) => true,
                _ => false
            }
        })
        .map(|(_, c)| { c })
        .collect::<Vec<_>>();

    if chars.len() == 10 {
        let sum = chars.iter().enumerate().fold(0, |total, (index, num)| {
            total + (10 - index) * match num {
                &'X' => 10,
                _ => num.to_string().parse().unwrap()
            }
        });

        if sum % 11 == 0 {
            return true;
        }
    }

    false
}
