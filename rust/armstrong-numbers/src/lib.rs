extern crate num;

// Me lazy
// https://users.rust-lang.org/t/how-to-convert-a-number-to-numeric-vec/10404
fn number_to_sorted_vec(n: u32) -> Vec<u32> {
    let mut digits = Vec::new();
    let mut n = n;
    while n > 9 {
        digits.push(n % 10);
        n = n / 10;
    }
    digits.push(n);
    digits.sort_unstable();
    digits.reverse();
    digits
}

pub fn is_armstrong_number(num: u32) -> bool {
    let digits = number_to_sorted_vec(num);

    let mut sum: u32 = 0;

    // Iterate from largest->smallest number, to encourage early-over and stop iterating asap
    for (_, digit) in digits.iter().enumerate() {
        sum += num::pow(*digit, digits.len());

        if sum > num {
            break;
        }
    };

    sum == num
}
