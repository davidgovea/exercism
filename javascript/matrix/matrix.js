module.exports = class Matrix {
  constructor(matrixString) {

    const rows = matrixString
      .split('\n')
      .map((rowString) => {
        return rowString.split(/\s+/).map(Number);
      });

    const cols = (rows[0] || [])
      .map((_, colIndex) => {
        return rows.map((_, rowIndex) => rows[rowIndex][colIndex])
      });

    this.rows = rows;
    this.columns = cols;
  }
}
