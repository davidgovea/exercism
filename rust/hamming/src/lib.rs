pub fn hamming_distance(s1: &str, s2: &str) -> Result<u64, ()> {
    match s1.len() == s2.len() {
        true => {
            let s1_chars: Vec<char> = s1.chars().collect();
            Ok(s2.chars().enumerate().fold(0, |dist, (i, c)| {
                match c == s1_chars[i] {
                    true => dist,
                    false => dist + 1,
                }
            }))
        },
        // false => Err("length mismatch")
        false => Err(()) // Weird that the result expects empty-tuple error..
    }
}
