#![feature(test)]
extern crate saddle_points;
extern crate test;

use test::Bencher;

use saddle_points::*;

#[bench]
fn bench_my_answer(b: &mut Bencher) {
    b.iter(|| {
        let input = vec![vec![9, 8, 7], vec![5, 3, 2], vec![6, 6, 7]];
        assert_eq!(vec![(1, 0)], find_saddle_points_first(&input));
        let input2 = vec![vec![], vec![], vec![]];
        let expected: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected, find_saddle_points_first(&input2));
        let input3 = vec![vec![1, 2, 3], vec![3, 1, 2], vec![2, 3, 1]];
        let expected2: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected2, find_saddle_points_first(&input3));
        let input4 = vec![vec![4, 5, 4], vec![3, 5, 5], vec![1, 5, 4]];
        assert_eq!(vec![(0, 1), (1, 1), (2, 1)], find_saddle_points_first(&input4));
        let input5 = vec![vec![8, 7, 9], vec![6, 7, 6], vec![3, 2, 5]];
        assert_eq!(vec![(2, 2)], find_saddle_points_first(&input5));
        let input6 = vec![vec![1, 5], vec![3, 6], vec![2, 7], vec![3, 8]];
        assert_eq!(vec![(0, 1)], find_saddle_points_first(&input6));
        let input7 = vec![vec![8, 7, 10, 7, 9], vec![8, 7, 13, 7, 9]];
        assert_eq!(vec![(0, 2)], find_saddle_points_first(&input7));
        let input8 = vec![vec![1], vec![3], vec![2], vec![3]];
        assert_eq!(vec![(0, 0)], find_saddle_points_first(&input8));
    })
}

#[bench]
fn bench_my_answer_2(b: &mut Bencher) {
    b.iter(|| {
        let input = vec![vec![9, 8, 7], vec![5, 3, 2], vec![6, 6, 7]];
        assert_eq!(vec![(1, 0)], find_saddle_points(&input));
        let input2 = vec![vec![], vec![], vec![]];
        let expected: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected, find_saddle_points(&input2));
        let input3 = vec![vec![1, 2, 3], vec![3, 1, 2], vec![2, 3, 1]];
        let expected2: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected2, find_saddle_points(&input3));
        let input4 = vec![vec![4, 5, 4], vec![3, 5, 5], vec![1, 5, 4]];
        assert_eq!(vec![(0, 1), (1, 1), (2, 1)], find_saddle_points(&input4));
        let input5 = vec![vec![8, 7, 9], vec![6, 7, 6], vec![3, 2, 5]];
        assert_eq!(vec![(2, 2)], find_saddle_points(&input5));
        let input6 = vec![vec![1, 5], vec![3, 6], vec![2, 7], vec![3, 8]];
        assert_eq!(vec![(0, 1)], find_saddle_points(&input6));
        let input7 = vec![vec![8, 7, 10, 7, 9], vec![8, 7, 13, 7, 9]];
        assert_eq!(vec![(0, 2)], find_saddle_points(&input7));
        let input8 = vec![vec![1], vec![3], vec![2], vec![3]];
        assert_eq!(vec![(0, 0)], find_saddle_points(&input8));
    })
}

#[bench]
fn bench_points_bkhl(b: &mut Bencher) {
    b.iter(|| {
        let input = vec![vec![9, 8, 7], vec![5, 3, 2], vec![6, 6, 7]];
        assert_eq!(vec![(1, 0)], find_saddle_points_bkhl(&input));
        let input2 = vec![vec![], vec![], vec![]];
        let expected: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected, find_saddle_points_bkhl(&input2));
        let input3 = vec![vec![1, 2, 3], vec![3, 1, 2], vec![2, 3, 1]];
        let expected2: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected2, find_saddle_points_bkhl(&input3));
        let input4 = vec![vec![4, 5, 4], vec![3, 5, 5], vec![1, 5, 4]];
        assert_eq!(vec![(0, 1), (1, 1), (2, 1)], find_saddle_points_bkhl(&input4));
        let input5 = vec![vec![8, 7, 9], vec![6, 7, 6], vec![3, 2, 5]];
        assert_eq!(vec![(2, 2)], find_saddle_points_bkhl(&input5));
        let input6 = vec![vec![1, 5], vec![3, 6], vec![2, 7], vec![3, 8]];
        assert_eq!(vec![(0, 1)], find_saddle_points_bkhl(&input6));
        let input7 = vec![vec![8, 7, 10, 7, 9], vec![8, 7, 13, 7, 9]];
        assert_eq!(vec![(0, 2)], find_saddle_points_bkhl(&input7));
        let input8 = vec![vec![1], vec![3], vec![2], vec![3]];
        assert_eq!(vec![(0, 0)], find_saddle_points_bkhl(&input8));
    })
}

#[bench]
fn bench_points_mark(b: &mut Bencher) {
    b.iter(|| {
        let input = vec![vec![9, 8, 7], vec![5, 3, 2], vec![6, 6, 7]];
        assert_eq!(vec![(1, 0)], find_saddle_points_mark(&input));
        let input2 = vec![vec![], vec![], vec![]];
        let expected: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected, find_saddle_points_mark(&input2));
        let input3 = vec![vec![1, 2, 3], vec![3, 1, 2], vec![2, 3, 1]];
        let expected2: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected2, find_saddle_points_mark(&input3));
        let input4 = vec![vec![4, 5, 4], vec![3, 5, 5], vec![1, 5, 4]];
        assert_eq!(vec![(0, 1), (1, 1), (2, 1)], find_saddle_points_mark(&input4));
        let input5 = vec![vec![8, 7, 9], vec![6, 7, 6], vec![3, 2, 5]];
        assert_eq!(vec![(2, 2)], find_saddle_points_mark(&input5));
        let input6 = vec![vec![1, 5], vec![3, 6], vec![2, 7], vec![3, 8]];
        assert_eq!(vec![(0, 1)], find_saddle_points_mark(&input6));
        let input7 = vec![vec![8, 7, 10, 7, 9], vec![8, 7, 13, 7, 9]];
        assert_eq!(vec![(0, 2)], find_saddle_points_mark(&input7));
        let input8 = vec![vec![1], vec![3], vec![2], vec![3]];
        assert_eq!(vec![(0, 0)], find_saddle_points_mark(&input8));
    })
}

#[bench]
fn bench_points_bmer(b: &mut Bencher) {
    b.iter(|| {
        let input = vec![vec![9, 8, 7], vec![5, 3, 2], vec![6, 6, 7]];
        assert_eq!(vec![(1, 0)], find_saddle_points_bmer(&input));
        let input2 = vec![vec![], vec![], vec![]];
        let expected: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected, find_saddle_points_bmer(&input2));
        let input3 = vec![vec![1, 2, 3], vec![3, 1, 2], vec![2, 3, 1]];
        let expected2: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected2, find_saddle_points_bmer(&input3));
        let input4 = vec![vec![4, 5, 4], vec![3, 5, 5], vec![1, 5, 4]];
        assert_eq!(vec![(0, 1), (1, 1), (2, 1)], find_saddle_points_bmer(&input4));
        let input5 = vec![vec![8, 7, 9], vec![6, 7, 6], vec![3, 2, 5]];
        assert_eq!(vec![(2, 2)], find_saddle_points_bmer(&input5));
        let input6 = vec![vec![1, 5], vec![3, 6], vec![2, 7], vec![3, 8]];
        assert_eq!(vec![(0, 1)], find_saddle_points_bmer(&input6));
        let input7 = vec![vec![8, 7, 10, 7, 9], vec![8, 7, 13, 7, 9]];
        assert_eq!(vec![(0, 2)], find_saddle_points_bmer(&input7));
        let input8 = vec![vec![1], vec![3], vec![2], vec![3]];
        assert_eq!(vec![(0, 0)], find_saddle_points_bmer(&input8));
    })
}

#[bench]
fn bench_points_pmj(b: &mut Bencher) {
    b.iter(|| {
        let input = vec![vec![9, 8, 7], vec![5, 3, 2], vec![6, 6, 7]];
        assert_eq!(vec![(1, 0)], find_saddle_points_pmj(&input));
        let input2 = vec![vec![], vec![], vec![]];
        let expected: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected, find_saddle_points_pmj(&input2));
        let input3 = vec![vec![1, 2, 3], vec![3, 1, 2], vec![2, 3, 1]];
        let expected2: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected2, find_saddle_points_pmj(&input3));
        let input4 = vec![vec![4, 5, 4], vec![3, 5, 5], vec![1, 5, 4]];
        assert_eq!(vec![(0, 1), (1, 1), (2, 1)], find_saddle_points_pmj(&input4));
        let input5 = vec![vec![8, 7, 9], vec![6, 7, 6], vec![3, 2, 5]];
        assert_eq!(vec![(2, 2)], find_saddle_points_pmj(&input5));
        let input6 = vec![vec![1, 5], vec![3, 6], vec![2, 7], vec![3, 8]];
        assert_eq!(vec![(0, 1)], find_saddle_points_pmj(&input6));
        let input7 = vec![vec![8, 7, 10, 7, 9], vec![8, 7, 13, 7, 9]];
        assert_eq!(vec![(0, 2)], find_saddle_points_pmj(&input7));
        let input8 = vec![vec![1], vec![3], vec![2], vec![3]];
        assert_eq!(vec![(0, 0)], find_saddle_points_pmj(&input8));
    })
}

#[bench]
fn bench_points_ryym(b: &mut Bencher) {
    b.iter(|| {
        let input = vec![vec![9, 8, 7], vec![5, 3, 2], vec![6, 6, 7]];
        assert_eq!(vec![(1, 0)], find_saddle_points_ryym(&input));
        let input2 = vec![vec![], vec![], vec![]];
        let expected: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected, find_saddle_points_ryym(&input2));
        let input3 = vec![vec![1, 2, 3], vec![3, 1, 2], vec![2, 3, 1]];
        let expected2: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected2, find_saddle_points_ryym(&input3));
        let input4 = vec![vec![4, 5, 4], vec![3, 5, 5], vec![1, 5, 4]];
        assert_eq!(vec![(0, 1), (1, 1), (2, 1)], find_saddle_points_ryym(&input4));
        let input5 = vec![vec![8, 7, 9], vec![6, 7, 6], vec![3, 2, 5]];
        assert_eq!(vec![(2, 2)], find_saddle_points_ryym(&input5));
        let input6 = vec![vec![1, 5], vec![3, 6], vec![2, 7], vec![3, 8]];
        assert_eq!(vec![(0, 1)], find_saddle_points_ryym(&input6));
        let input7 = vec![vec![8, 7, 10, 7, 9], vec![8, 7, 13, 7, 9]];
        assert_eq!(vec![(0, 2)], find_saddle_points_ryym(&input7));
        let input8 = vec![vec![1], vec![3], vec![2], vec![3]];
        assert_eq!(vec![(0, 0)], find_saddle_points_ryym(&input8));
    })
}

#[bench]
fn bench_points_miken(b: &mut Bencher) {
    b.iter(|| {
        let input = vec![vec![9, 8, 7], vec![5, 3, 2], vec![6, 6, 7]];
        assert_eq!(vec![(1, 0)], find_saddle_points_miken(&input));
        let input2 = vec![vec![], vec![], vec![]];
        let expected: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected, find_saddle_points_miken(&input2));
        let input3 = vec![vec![1, 2, 3], vec![3, 1, 2], vec![2, 3, 1]];
        let expected2: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected2, find_saddle_points_miken(&input3));
        let input4 = vec![vec![4, 5, 4], vec![3, 5, 5], vec![1, 5, 4]];
        assert_eq!(vec![(0, 1), (1, 1), (2, 1)], find_saddle_points_miken(&input4));
        let input5 = vec![vec![8, 7, 9], vec![6, 7, 6], vec![3, 2, 5]];
        assert_eq!(vec![(2, 2)], find_saddle_points_miken(&input5));
        let input6 = vec![vec![1, 5], vec![3, 6], vec![2, 7], vec![3, 8]];
        assert_eq!(vec![(0, 1)], find_saddle_points_miken(&input6));
        let input7 = vec![vec![8, 7, 10, 7, 9], vec![8, 7, 13, 7, 9]];
        assert_eq!(vec![(0, 2)], find_saddle_points_miken(&input7));
        let input8 = vec![vec![1], vec![3], vec![2], vec![3]];
        assert_eq!(vec![(0, 0)], find_saddle_points_miken(&input8));
    })
}

#[bench]
fn bench_points_oliveruv(b: &mut Bencher) {
    b.iter(|| {
        let input = vec![vec![9, 8, 7], vec![5, 3, 2], vec![6, 6, 7]];
        assert_eq!(vec![(1, 0)], find_saddle_points_oliveruv(&input));
        let input2 = vec![vec![], vec![], vec![]];
        let expected: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected, find_saddle_points_oliveruv(&input2));
        let input3 = vec![vec![1, 2, 3], vec![3, 1, 2], vec![2, 3, 1]];
        let expected2: Vec<(usize, usize)> = Vec::new();
        assert_eq!(expected2, find_saddle_points_oliveruv(&input3));
        let input4 = vec![vec![4, 5, 4], vec![3, 5, 5], vec![1, 5, 4]];
        assert_eq!(vec![(0, 1), (1, 1), (2, 1)], find_saddle_points_oliveruv(&input4));
        let input5 = vec![vec![8, 7, 9], vec![6, 7, 6], vec![3, 2, 5]];
        assert_eq!(vec![(2, 2)], find_saddle_points_oliveruv(&input5));
        let input6 = vec![vec![1, 5], vec![3, 6], vec![2, 7], vec![3, 8]];
        assert_eq!(vec![(0, 1)], find_saddle_points_oliveruv(&input6));
        let input7 = vec![vec![8, 7, 10, 7, 9], vec![8, 7, 13, 7, 9]];
        assert_eq!(vec![(0, 2)], find_saddle_points_oliveruv(&input7));
        let input8 = vec![vec![1], vec![3], vec![2], vec![3]];
        assert_eq!(vec![(0, 0)], find_saddle_points_oliveruv(&input8));
    })
}
