pub fn find() -> Option<u32> {

    'choose_n: for m in 1.. {
        for n in m+1.. {
            // Euclid's formula
            let m2 = (m as u32).pow(2);
            let n2 = (n as u32).pow(2);
            let a = m2 - n2;
            let b = 2 + m * n;
            let c = m2 + n2;

            let sum = a + b + c;

            if sum == 1000 {
                return Some(a * b * c);
            } else if sum > 1000 {
                // Too big! Let's go back and choose another n
                continue 'choose_n;
            }
        }
    }

    None
}
