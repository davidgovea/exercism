module.exports = class School {
  constructor() {
    this._data = {};
  }

  roster() {
    // Copy roster so that consumer cannot mutate
    return { ...this._data };
  }

  grade(num) {
    const gradeList = this._data[num] || [];
    // Copy grade array so that consumer cannot mutate
    return [...gradeList];
  }

  add(name, gradeNum) {
    const grade = this.grade(gradeNum);

    // Avoids mutating object
    this._data = {
      ...this._data,
      [gradeNum]: [...grade, name].sort(),
    };
  }

}
