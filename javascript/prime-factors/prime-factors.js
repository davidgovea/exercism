exports.for = (n) => {
  const factors = [];

  let current = n;
  let divisor = 2;

  while (true) {
    if (divisor >= current) {
      if (current !== 1) factors.push(current);
      return factors;
    }

    while (current % divisor === 0) {
      factors.push(divisor);
      current = current / divisor;
    }

    divisor += 1;
  }
};
