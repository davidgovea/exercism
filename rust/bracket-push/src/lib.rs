enum Bracket {
    Open,
    Close
}

fn classify_bracket(c: char) -> Option<Bracket> {
    match c {
        '[' | '(' | '{' => Some(Bracket::Open),
        ']' | ')' | '}' => Some(Bracket::Close),
        _ => None
    }
}

fn get_closing_bracket(b: char) -> Option<char> {
    match b {
        '[' => Some(']'),
        '(' => Some(')'),
        '{' => Some('}'),
        _ => None,
    }
}

pub fn brackets_are_balanced(string: &str) -> bool {    
    let bracket_stack_result = string
        .chars()
        // try_fold used to allow short-circuit return on nesting error
        .try_fold(Vec::new(), |mut stack, c| {
            match classify_bracket(c) {
                Some(Bracket::Open) => {
                    let next_expected_closing = get_closing_bracket(c).unwrap();
                    stack.push(next_expected_closing);
                },
                Some(Bracket::Close) => {
                    let expected_closing = stack.pop();
                    if Some(c) != expected_closing {
                        return Err("Bad nesting")
                    }
                }
                _ => ()
            };
            Ok(stack)
        });
    
    // Make sure stack is empty, and no nesting errors encountered
    match bracket_stack_result {
        Ok(stack) => stack.len() == 0,
        Err(_) => false
    }
}
