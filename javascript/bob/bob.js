module.exports = class Bob {
  hey(text = '') {
    // Strip whitespace at end
    text = text.replace(/\s+$/g, '');

    const isQuestion = text[text.length - 1] === '?';
    // Lowercase will be different if there are any alpha chars
    const isYelling = text.toUpperCase() === text
                   && text.toLowerCase() !== text;

    let response = 'Fine. Be that way!';

    if (isYelling) {
      response = 'Whoa, chill out!';

    } else if (isQuestion) {
      response = 'Sure.';

    } else if (text) {
      response = 'Whatever.';
    }

    return response;
  }
}
