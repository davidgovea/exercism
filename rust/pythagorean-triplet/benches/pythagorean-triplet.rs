#![feature(test)]
extern crate pythagorean_triplet;
extern crate test;

use test::Bencher;

/*
Results on my machine:

without early continue: 635,377 ns/iter (+/- 117,637)
with early continue: 721,473 ns/iter (+/- 94,478) WHAT?!
euclid's formula: 859 ns/iter (+/- 447)
*/

#[bench]
fn bench_answer(b: &mut Bencher) {
    b.iter(|| pythagorean_triplet::find())
}
