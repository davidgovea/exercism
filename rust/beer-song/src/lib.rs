pub fn verse(n: i32) -> String {
    let current = match n {
        0 => "No more bottles".to_string(),
        1 => "1 bottle".to_string(),
        _ => format!("{} bottles", n),
    };

    let action = match n {
        0 => "Go to the store and buy some more",
        1 => "Take it down and pass it around",
        _ => "Take one down and pass it around"
    };

    let next = match n {
        0 => "99 bottles".to_string(),
        1 => "no more bottles".to_string(),
        2 => "1 bottle".to_string(),
        _ => format!("{} bottles", n - 1),
    };

    format!("{} of beer on the wall, {} of beer.\n{}, {} of beer on the wall.\n", current, current.to_lowercase(), action, next)
}

pub fn sing(start: i32, end: i32) -> String {
    (end..start+1).rev()
        .map(|i| { verse(i) })
        .collect::<Vec<_>>()
        .join("\n")
}
