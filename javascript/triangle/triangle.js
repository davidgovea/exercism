export default class Triangle {
  constructor(...edgeLengths) {
    this.edges = edgeLengths;
  }

  get sortedEdges() {
    return [...this.edges].sort((a, b) => a - b);
  }

  get uniqueEdges() {
    return new Set(this.edges).size;
  }

  // Maybe this should happen in the constructor?
  //  .. designing to the tests here
  checkValidity() {
    const [a, b, c] = this.sortedEdges;

    if ((a + b) <= c) {
      throw new Error('illegal triangle');
    }
  }

  kind() {
    this.checkValidity();
    const { uniqueEdges } = this;
    if (uniqueEdges === 1) {
      return 'equilateral';
    }
    if (uniqueEdges === 2) {
      return 'isosceles';
    }
    return 'scalene';
  }
}
