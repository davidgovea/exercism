extern crate rand;
use rand::{Rng,OsRng};

pub fn private_key(p: u64) -> u64 {
    let mut rng = OsRng::new().unwrap();
    rng.gen_range(2, p)
}

pub fn public_key(p: u64, g: u64, a: u64) -> u64 {
    // a_pub = g**a mod p
    modular_exp(g, a, p)
}

pub fn secret(p: u64, b_pub: u64, a: u64) -> u64 {
    // s = b_pub**a mod p
    modular_exp(b_pub, a, p)
}

fn modular_exp(b: u64, e: u64, m: u64) -> u64 {
    // https://en.wikipedia.org/wiki/Modular_exponentiation#Memory-efficient_method
    let mut e_ = 0;
    let mut c = 1;

    while e_ < e {
        e_ += 1;
        c = (c * b) % m;
    }
    c
}
