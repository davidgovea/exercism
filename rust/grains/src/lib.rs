#![feature(inclusive_range_syntax)]

pub fn square(s: u32) -> u64 {
    match s > 0 && s < 65 {
        true => 2u64.pow(s-1),
        false => panic!("Square must be between 1 and 64")
    }
}

pub fn total() -> u64 {
    (1...64).map(square).sum()
}
