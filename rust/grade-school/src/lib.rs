use std::collections::BTreeMap;

pub struct School {
    data: BTreeMap<u32, Vec<String>>
}

impl School {
    pub fn new() -> School {
        School { data: BTreeMap::new() }
    }

    pub fn add(&mut self, grade: u32, student: &str) {
        match self.data.contains_key(&grade) {
            true => {
                let mut current_students = self.data.get(&grade).unwrap().to_vec();
                current_students.push(student.to_string());
                current_students.sort();
                self.data.insert(grade, current_students);
            },
            false => {
                self.data.insert(grade, vec![student.to_string()]);
            }
        };
    }

    pub fn grades(&self) -> Vec<u32> {
        self.data.keys().cloned().collect()
    }

    pub fn grade(&self, grade: u32) -> Option<Vec<String>> {
        match self.data.get(&grade) {
            Some(d) => Some(d.to_vec()),
            None => None
        }
    }
}
