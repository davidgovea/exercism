use std::collections::HashMap;

const KNOWN_NUCLEOTIDES: [char; 4] = ['A', 'C', 'G', 'T'];

fn check_nucleotide_validity(nucleotide: char) -> Result<(), char> {
    match nucleotide {
        'A' | 'C' | 'T' | 'G' => Ok(()),
        _ => Err(nucleotide),
    }
}

pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {
    check_nucleotide_validity(nucleotide)?;

    dna.chars().try_fold(0, |count, c| {
        check_nucleotide_validity(c)?;
        Ok(match c == nucleotide {
            true => count + 1,
            false => count,
        })
    })
}

fn create_empty_hashmap() -> HashMap<char, usize> {
    KNOWN_NUCLEOTIDES
        .iter()
        .fold(HashMap::new(), |mut map: HashMap<char, usize>, &c| {
            map.insert(c, 0);
            map
        })
}
pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    dna.chars().try_fold(
        create_empty_hashmap(),
        |mut map: HashMap<char, usize>, c| {
            check_nucleotide_validity(c)?;
            *map.entry(c).or_insert(0) += 1;
            Ok(map)
        },
    )
}
