module.exports = class Pangram {
  constructor(text = '') {
    this.text = text;
  }

  isPangram() {
    const characters = this.text.toLowerCase().replace(/[^a-z]/g, '').split('');
    return new Set(characters).size === 26;
  }
}
