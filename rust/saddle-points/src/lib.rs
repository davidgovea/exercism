#![feature(iterator_for_each)]

pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {

    // Keep track of computed column mins
    let mut col_mins = vec![None; input[0].len()];

    input.iter().enumerate().fold(vec![], |mut list, (row_index, row)| {
        match row.iter().max() {
            Some(row_max) => {
                row.iter().enumerate().for_each(|(col_index, el)| {
                    if el == row_max {
                        // Only calculate if we are at a row max
                        let col_min = match col_mins[col_index] {
                            // This is our first time computing this row
                            None => {
                                let col_min = input.iter().map(|row| row[col_index]).min().unwrap();
                                col_mins[col_index] = Some(col_min);
                                col_min
                            },
                            // We've seen it before
                            Some(n) => n
                        };

                        if row[col_index] == col_min {
                            list.push((row_index, col_index));
                        }
                    }
                });
            },
            None => {}
        };

        list
    })
}
