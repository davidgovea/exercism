extern crate nth_prime as np;

// Naive solution. Using previous exercise crate for local dep experience.
//  Plan to try a congruence-of-squares type method in the future
pub fn factors(n: usize) -> Vec<usize> {

    let mut result: Vec<usize> = vec![];

    let mut current = n;

    for i in 1.. {
        if np::is_prime(current) {
            if current != 1 { result.push(current) }
            return result;
        }

        let prime = np::nth(i).unwrap();
        while current % prime == 0 {
            result.push(prime);
            current = current / prime;
        }
    }

    result
}
