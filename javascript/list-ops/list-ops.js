module.exports = class List {
  constructor(values = []) {
    this.values = [...values];
  }

  append(list) {
    this.values = [...this.values, ...list.values];
    return this;
  }

  concat(list) {
    return new List([...this.values, ...list.values]);
  }

  length() {
    return this.values.length;
  }

  filter(filterFn = () => true) {
    const values = [];
    for (const value of this.values) {
      if (filterFn(value)) values.push(value);
    }
    this.values = values;
    return this;
  }

  map(mapFn = (v) => v) {
    const values = [];
    for (const value of this.values) {
      values.push(mapFn(value));
    }
    this.values = values;
    return this;
  }

  foldl(foldFn, initialValue) {
    let currentValue = initialValue;
    for (const value of this.values) {
      currentValue = foldFn(value, currentValue);
    }
    return currentValue;
  }

  foldr(foldFn, initialValue) {
    let currentValue = initialValue;
    for (let i = this.values.length; i > 0; i--) {
      const value = this.values[i - 1];
      currentValue = foldFn(value, currentValue);
    }
    return currentValue;
  }

  reverse() {
    const values = [];
    for (let i = this.values.length; i > 0; i--) {
      values.push(this.values[i - 1]);
    }
    return new List(values);
  }

}
