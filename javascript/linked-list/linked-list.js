// So umm.. the test suite didn't require any features beyond a regular
//  array.. So I just proxied the calls through and added count/delete.
//  Where are the next / prev feature specs?
module.exports = class LinkedList {
  constructor() {
    this.list = [];
  }

  pop() {
    return this.list.pop.call(this.list);
  }

  push() {
    return this.list.push.call(this.list, ...arguments);
  }

  shift() {
    return this.list.shift.call(this.list);
  }

  unshift() {
    return this.list.unshift.call(this.list, ...arguments);
  }

  count() {
    return this.list.length;
  }

  delete(el) {
    const index = this.list.indexOf(el);
    if (index !== -1) {
      this.list.splice(index, 1);
    }
  }
}
