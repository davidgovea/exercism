pub fn reply(mut message: &str) -> &str {
    message = message.trim();
    let is_question = message.ends_with("?");
    let is_yelling = message.to_uppercase() == message && message.to_lowercase() != message;
    let is_anything = !message.is_empty();

    match (is_anything, is_question, is_yelling) {
        (true, false, false) => "Whatever.",
        (true, false, true) => "Whoa, chill out!",
        (true, true, false) => "Sure.",
        (true, true, true) => "Calm down, I know what I'm doing!",
        (false, _, _) => "Fine. Be that way!",
    }
}
