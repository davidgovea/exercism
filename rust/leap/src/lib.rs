pub fn is_leap_year(year: i32) -> bool {
    let is_plain_leap = year % 4 == 0;
    let is_exception_year = year % 100 == 0 && year % 400 != 0;

    return is_plain_leap && !is_exception_year;
}
