#[derive(Debug, PartialEq)]
pub enum Error {
    NotEnoughPinsLeft,
    GameComplete,
}

pub struct BowlingGame {
    frames: Vec<BowlingFrame>
}

#[derive(Debug)]
struct BowlingFrame {
    rolls: (Option<u16>, Option<u16>),
    fills: (Option<u16>, Option<u16>),
}

impl BowlingFrame {
    pub fn is_rolled(&self) -> bool {
        match self.rolls {
            // Has 2 rolls
            (_, Some(_)) => true,
            // Only 1 roll, but it's a strike
            (Some(first), None) if first == 10 => true,
            _ => false,
        }
    }

    pub fn needs_10th_fill_rolls(&self) -> bool {
        match (self.lookahead_count(), self.fills) {
            // Strike in 10th, with 2 fills. Done!
            (2, (Some(_), Some(_))) => false,
            // Spare in 10th, with 1 fill. Done!
            (1, (Some(_), None)) => false,
            // No lookaheads needed (open frame)
            (0, _) => false,
            _ => true,
        }
    }

    pub fn score(&self) -> u16 {
        match self.rolls {
            (Some(first), None) => first,
            (Some(first), Some(second)) => first + second,
            _ => 0,
        }
    }

    pub fn lookahead_count(&self) -> usize {
        match self.rolls {
            // strike: 2 lookahead
            (Some(first), None) if first == 10 => 2,
            // spare: 1 lookahead
            (Some(first), Some(second)) if first + second == 10 => 1,
            _ => 0,
        }
    }

    pub fn add_roll(&mut self, pins: u16) -> Result<(), Error> {
        self.validate_pins(self.rolls.0, pins)?;
        self.rolls = match self.rolls {
            (None, _) => (Some(pins), None),
            (Some(first), _) => (Some(first), Some(pins)),
        };

        Ok(())
    }

    pub fn add_fill_roll(&mut self, pins: u16) -> Result<(), Error> {
        self.validate_pins(self.fills.0, pins)?;
        self.fills = match self.fills {
            (None, _) => (Some(pins), None),
            (Some(first), _) => (Some(first), Some(pins)),
        };
        Ok(())
    }

    fn validate_pins(&self, first_throw: Option<u16>, pins: u16) -> Result<(), Error> {
        match (first_throw, pins) {
            (_, p) if p > 10 => {
                return Err(Error::NotEnoughPinsLeft);
            },
            (Some(first), pins) if first != 10 && first + pins > 10 => {
                return Err(Error::NotEnoughPinsLeft);
            },
            _ => ()
        };
        Ok(())
    }
}

impl BowlingGame {
    pub fn new() -> Self {
        BowlingGame {
            frames: vec![],
        }
    }

    pub fn roll(&mut self, pins: u16) -> Result<(), Error> {
        let frame_count = self.frames.len();

        let frame = match self.frames.last() {
            // In-progress frame. Add next roll
            Some(frame) if !frame.is_rolled() => {
                let mut current_frame = self.frames.pop().unwrap();
                current_frame.add_roll(pins)?;
                current_frame
            },
            // 10-th frame that needs fill roll(s)
            Some(frame) if frame_count == 10 && frame.needs_10th_fill_rolls() => {
                let mut current_frame = self.frames.pop().unwrap();
                current_frame.add_fill_roll(pins)?;
                current_frame
            },
            // Fresh frame
            _ if frame_count < 10 => {
                let mut new_frame = BowlingFrame {
                    rolls: (None, None),
                    fills: (None, None)
                };
                new_frame.add_roll(pins)?;
                new_frame
            },
            // Anything else is an error
            _ => {
                return Err(Error::GameComplete);
            }
        };

        self.frames.push(frame);
        Ok(())
    }

    pub fn score(&self) -> Option<u16> {
        // Check for 10 rolled frames
        let complete_frames = self.frames
            .iter()
            .filter(|f| f.is_rolled())
            .collect::<Vec<_>>();
        if complete_frames.len() != 10 {
            return None;
        }

        // Check for fill rolls - need complete score
        let tenth_frame = complete_frames.last().unwrap();
        if tenth_frame.needs_10th_fill_rolls() {
            return None;
        }

        let score = self.frames
            .iter()
            .enumerate()
            .fold(0, |score, (index, frame)| {

                let lookahead_count = frame.lookahead_count();
                
                let mut lookahead_rolls = vec![];
                let mut add_lookahead = |pins: Option<u16>| {
                    let needs_more_rolls = lookahead_rolls.len() < lookahead_count;
                    match (needs_more_rolls, pins) {
                        (true, Some(p)) => lookahead_rolls.push(p),
                        _ => ()
                    };
                };

                let mut lookahead_iter = self.frames.iter().skip(index);
                let mut lookahead_frame = lookahead_iter.next();
                while let Some(frame) = lookahead_frame {
                    add_lookahead(frame.fills.0);
                    add_lookahead(frame.fills.1);

                    lookahead_frame = lookahead_iter.next();
                    if let Some(frame) = lookahead_frame {
                        add_lookahead(frame.rolls.0);
                        add_lookahead(frame.rolls.1);
                    }
                }

                score + frame.score() + lookahead_rolls.iter().sum::<u16>()
            });

        Some(score)
    }
}
