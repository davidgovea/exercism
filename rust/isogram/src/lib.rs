use std::collections::HashMap;

pub fn check(input: &str) -> bool {

    // HashSet would be better than HashMap
    let mut seen_chars: HashMap<String, bool> = HashMap::new();

    !input.chars().any(|c| {
        if c.is_alphabetic() {
            let lower = c.to_lowercase().collect();
            match seen_chars.get(&lower) {
                Some(_) => {
                    return true;
                },
                None => {
                    seen_chars.insert(lower, true);
                }
            }
        }
        false
    })
}
