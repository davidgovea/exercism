const A_CODE = "A".charCodeAt(0);
const Z_CODE = "Z".charCodeAt(0);
const KEY_SPACE = Z_CODE - A_CODE + 1;

module.exports = class RotationalCipher {
  rotate(text = '', count = 0) {
    return text
      .split('')
      .reduce((output, char) => {
        let newChar = char;
        const upperChar = char.toUpperCase();
        const originallyLower = upperChar !== char;
        const charCode = upperChar.charCodeAt(0);

        if (charCode >= A_CODE && charCode <= Z_CODE) {
          newChar = String.fromCharCode((charCode - A_CODE + count) % KEY_SPACE + A_CODE);
          if (originallyLower) {
            newChar = newChar.toLowerCase();
          }
        }

        return output + newChar;
      }, '')
  }
}
